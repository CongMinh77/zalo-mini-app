import React from "react";
import {
  Avatar,
  List,
  Text,
  Box,
  Page,
  Button,
  Icon,
  useNavigate,
  Input,
} from "zmp-ui";
import { useRecoilValue } from "recoil";
import { userState } from "../state";
import { userInfo } from "zmp-sdk";

const LoginPage = () => {
  const user = useRecoilValue<userInfo>(userState);
  const navigate = useNavigate();
  return (
    <Page className="page">
      <Box
        flex
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <div className="section-container">
          <Box>
            <Input id="name" label="Name" type="text" placeholder="username" />
            <Input
              id="password"
              label="Password"
              type="password"
              placeholder="password"
            />
          </Box>
        </div>
      </Box>
    </Page>
  );
};

export default LoginPage;
