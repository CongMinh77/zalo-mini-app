
import { atom } from "recoil";
import { userInfo } from 'zmp-sdk';

export const userState = atom<userInfo>({
  key: "user",
  default: {
    id: '123456789',
    name: 'Minhnc',
    avatar: 'ZA',
  }
})
