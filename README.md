# agilearn-dev

## ZMP CLI Options

ZMP app created with following options:

```
{
  "cwd": "/Users/quan/Documents/NCM/my-app",
  "newProject": true,
  "name": "agilearn-dev",
  "package": "zmp-ui",
  "framework": "react-typescript",
  "cssPreProcessor": "scss",
  "template": "single-view",
  "theming": {
    "color": "#007aff",
    "darkTheme": false,
    "fillBars": false
  },
  "customBuild": false,
  "includeTailwind": false,
  "stateManagement": "recoil"
}
```

## NPM Scripts

* 🔥 `start` - run development server
* 🙏 `deploy` - deploy mini app for production